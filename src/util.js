function factorial(n) {
  if (n === 1 || n === 0) return 1;
  return n * factorial(n - 1);
}

function div_check(n) {
  return n % 7 === 0 || n % 5 === 0;
}

const names = {
  Brandon: {
    name: "Lebron James",
    age: 38,
  },
  Steve: {
    name: "Steve Harvey",
    age: 22,
  },
};

module.exports = {
  factorial: factorial,
  div_check: div_check,
  names,
};
